#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/string.h>
#define BUFSIZE 2048
static dev_t first;
static struct cdev c_dev; 
static struct class *cl;
static struct proc_dir_entry* proc_file;
static int store = 0;
int is_number(char v){
    int value = 0;
    if(sscanf(&v, "%d", &value) == 0){
    	return 0;
    }
    else{
    	return 1;
    }
    
}
void buf_set_zero(char* buf){
    int len = strlen(buf);
    int i = 0;
    for(;i < len ; i++){
    	buf[i] = 0;
    }
}
void parse_buf(char* buf){
    long value = 0;
    int len = strlen(buf);
    char num[10];
    int current_num = 0;
    int is_previous_num = 0;
    int i = 0;
    int is_one_symb = 0;
    int is_previous_minus = 0;
    for(; i < len; i++){
        if(is_number(buf[i]) == 1){
            if(is_previous_num == 0){
            	current_num = 0;
            	if(is_previous_minus){
                    num[current_num] = '-';
                    is_previous_minus = 0;
                    current_num++;
                }
                num[current_num] = buf[i];
                is_previous_num = 1;
                is_one_symb = 1;
            }
            else{
                is_one_symb = 0;
                current_num++;
                num[current_num] = buf[i]; 
            }
        }else{
       	    if(buf[i] == '-') is_previous_minus = 1;
       	    else is_previous_minus = 0;
            if(current_num != 0 || is_one_symb == 1){
                sscanf(num, "%d", &value);
                store+= value;
                buf_set_zero(num);
                current_num = 0;
                is_one_symb = 0;
            }
            is_previous_num = 0;
        }  
    }
    if(current_num != 0 || is_one_symb == 1){
                sscanf(num, "%d", &value);
                store += value;
                current_num = 0;
                is_previous_num = 0;
    }
}
static ssize_t proc_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  if(*off > 0 || len < BUFSIZE) return 0;
  printk(KERN_INFO "Proc: read()\n");
  char l_buf[BUFSIZE];
  sprintf(l_buf, "%d\n", store);
  int b_len = strlen(l_buf);
  if(copy_to_user(buf, l_buf, b_len))
    return -EFAULT;
  *off = b_len;
  return b_len;
 
}
static ssize_t dr_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  if(*off > 0 || len < BUFSIZE) return 0;
  printk(KERN_INFO "Driver: read()\n");
  char l_buf[BUFSIZE];
  sprintf(l_buf, "%d\n", store);
  int b_len = strlen(l_buf);
  if(copy_to_user(buf, l_buf, b_len))
    return -EFAULT;
  
  *off = b_len;
  return b_len;
} 

static ssize_t dr_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{
  printk(KERN_INFO "Driver: write()\n");
   if(*off > 0 || len > BUFSIZE) return 0;
  
  printk(KERN_INFO "Driver: read()\n");
  char l_buf[BUFSIZE];
  sprintf(l_buf, "%ld\n", store);
  if(copy_from_user(l_buf, buf, len))
    return -EFAULT;
  parse_buf(l_buf);
  int b_len = strlen(l_buf); 
  *off = b_len;
  return b_len;
}

static struct file_operations mychdev_fops =
{
  .owner = THIS_MODULE,
  .read = dr_read,
  .write = dr_write
};
static const struct file_operations proc_fops =
{
  .owner = THIS_MODULE,
  .read = proc_read
};

static int __init ch_drv_init(void)
{
    proc_file = proc_create("var3", 0777, NULL, &proc_fops);

    
    printk(KERN_INFO "Hello!\n");
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    if (device_create(cl, NULL, first, NULL, "mychdev") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    return 0;
}
 
static void __exit ch_drv_exit(void)
{
    proc_remove(proc_file);
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    printk(KERN_INFO "Bye!!!\n");
}
 
module_init(ch_drv_init);
module_exit(ch_drv_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Author");
MODULE_DESCRIPTION("The first kernel module");

