# Лабораторная работа 2

Вариант 3

**Название:** "Разработка драйверов блочных устройств"

**Цель работы:** "Получить знания и навыки разработки драйверов блочных
устройств для операционной системы Linux."

## Описание функциональности драйвера

Драйвер создает виртуальный жесткий диск размером 50Мбайт, разделенный на один первичный
и один расширенный раздел, который состоит из 2 логических.

## Инструкция по сборке

1. make
2. make do

## Инструкция пользователя
1. Собрать модуль на своей машине использую инструкцию
2. Отформатировать диск необходимые разделы с помощью "mkfs.vfat /dev/mydisk[#]"
3. Монтировать диски в необхождимые точки с помощью "mount -t vfat mydisk[#] /..."
## Примеры использования
```
mint@mint:/dev$ sudo mkfs.vfat /dev/mydisk1
mint@mint:/dev$ sudo mount -t vfat mydisk1 /mnt/1
```
## Speed tests
#### Real to virtual
```
sudo rsync -av --progress --stats test.txt  /mnt/1/
sending incremental file list
test.txt
      5,242,880 100%   66.25MB/s    0:00:00 (xfr#1, to-chk=0/1)

Number of files: 1 (reg: 1)
Number of created files: 1 (reg: 1)
Number of deleted files: 0
Number of regular files transferred: 1
Total file size: 5,242,880 bytes
Total transferred file size: 5,242,880 bytes
Literal data: 5,242,880 bytes
Matched data: 0 bytes
File list size: 0
File list generation time: 0.001 seconds
File list transfer time: 0.000 seconds
Total bytes sent: 5,244,266
Total bytes received: 114

sent 5,244,266 bytes  received 114 bytes  10,488,760.00 bytes/sec
total size is 5,242,880  speedup is 1.00
```
#### Virtual to virtual
```
sudo rsync -av --progress --stats test.txt  /mnt/2/
sending incremental file list
test.txt
      5,242,880 100%   72.17MB/s    0:00:00 (xfr#1, to-chk=0/1)

Number of files: 1 (reg: 1)
Number of created files: 1 (reg: 1)
Number of deleted files: 0
Number of regular files transferred: 1
Total file size: 5,242,880 bytes
Total transferred file size: 5,242,880 bytes
Literal data: 5,242,880 bytes
Matched data: 0 bytes
File list size: 0
File list generation time: 0.001 seconds
File list transfer time: 0.000 seconds
Total bytes sent: 5,244,240
Total bytes received: 35

sent 5,244,240 bytes  received 35 bytes  10,488,550.00 bytes/sec
total size is 5,242,880  speedup is 1.00
```
